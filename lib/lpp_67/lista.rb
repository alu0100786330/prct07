include Enumerable
include Comparable

Node = Struct.new(:value, :next)

class Lista
    attr_reader :cabeza
    attr_reader :final
    
    def initialize()
        @cabeza=nil
        @final=nil
    end
    
    
    def extract_inicio()
       if @cabeza == @final
          raise RuntimeError,"No se puede extraer porque debe existir una lista con su cabeza"
	    else
	     @cabeza = @cabeza[:next]
       end
        
    end
    
    
    
    def empty()
       if @cabeza == nil
           true 
       else
           false
       end
    end    
    
    
    def insert_empty(nodo)
       node = Node.new(nodo) 
       @cabeza = node
       @final = node
    end
    
    def insert_end(nodo)
        if (empty())
            insert_empty(nodo)
        else
            node = Node.new(nodo)
            @final[:next] = node
            @final = @final[:next]
        end
    end
    
    
    def to_s()
       x="" 
       i = @cabeza
       while i != nil
           if i[:next] != nil 
               x << i[:value].to_s << " "
           else
               x << i[:value].to_s
           end
           i = i[:next]
       end
       "#{x}"
    end
    

    def each()
        x = ""
        nodo = @cabeza
        while nodo != nil
            x << nodo[:value] << " "  
            nodo = nodo[:next]
        end
        "#{x}"
    end
end